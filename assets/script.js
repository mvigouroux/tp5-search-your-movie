// variable qui récupère la valeur de l'input
const input = document.getElementById('in');
input.value = '';
input.addEventListener('click',resetInput);
//input.addEventListener('keypress',resetScreen);

//variable boutton submit
const submit = document.getElementById('search');
submit.addEventListener('click',()=>{
  searchAnyMovie()
});



//reset la barre d'input à chanque nouvelle recherche
function resetInput(){
  input.value = '';
  actualPage= 1;
}; 


//variables pagination
 
let actualPage = 1;

const prevButton = document.getElementById('previous');
prevButton.addEventListener('click', () =>{
  previous()
});

const nextButton = document.getElementById('nextPage');
nextButton.addEventListener('click', () =>{
  nextPage()
});


//requête pour chercher la valeur de l'input
let searchAnyMovie = () => {
  return  fetch(`https://api.themoviedb.org/3/search/movie?api_key=${key}&language=fr&query=${input.value}&include_adult=false&page=${actualPage}`)
  .then(function(response) {
    // Etape 1 : Extraction des données
    return response.json();
  }).then(function(transform) {
      // Etape 2 : Utilisation des données
      let films = transform.results;
     
      showMovies(films)
      showPageInfo();
    });
};

//afficher les éléments
let showMovies = (films) =>{
  //récupérer le résultat de la promise pour les mettre dans un tableau
  
  let cardDeck = document.getElementById('cardDeck');
  cardDeck.innerHTML = "";

  //placer chaque index de array dans une card
  for (let i = 0; i < films.length; i++) {
    let film = films[i];

    if(i < films.length){
      let card = document.createElement('div');
      card.classList.add('card');
      card.innerHTML=`
      <img class="card-img-top" src="https://image.tmdb.org/t/p/w500/${film.poster_path}" alt="Affiche du film ${film.title}">
      <div class="card-body">
        <h5 class="card-title" itemprop="name">${film.title}</h5>
      </div>
      </div>
      `
      cardDeck.append(card);
      
      card.addEventListener('click', function(){
        localStorage.setItem('filmId', film.id);
        window.location="pageArticle.html"
      });
    }
  }
 
};

//fonctions pagination
let previous = () =>{
  actualPage-=1;
  searchAnyMovie(); 
};

let nextPage = () =>{
  actualPage+=1;
  searchAnyMovie(); 
};

let showPageInfo = () =>{
  document.getElementById('pageInfo').innerHTML =`Page ${actualPage}`
};

