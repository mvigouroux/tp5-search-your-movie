let filmId = localStorage.getItem('filmId');


const buttonHome = document.getElementById('accueil');
      
buttonHome.addEventListener('click', function(){
	window.location='index.html';
});

detailsFilm();

function detailsFilm(){
	fetch(`https://api.themoviedb.org/3/movie/${filmId}?api_key=${key}&language=fr&append_to_response=videos,images,credits`)
		.then(function(response) {
			// Etape 1 : Extraction des données
			return response.json();
		}).then(function(transform) {
			// Etape 2 : Utilisation des données
			console.log(transform);
        
			let containerAffiche = document.getElementById('poster');
			containerAffiche.innerHTML=`
        <img itemprop="image" src="https://image.tmdb.org/t/p/w500/${transform.poster_path}" alt="Affiche du film ${transform.title}">
        `;
    
			let containerDescription = document.getElementById('description');
			containerDescription.innerHTML=`
			<h2 class="col-7">${transform.title}</h5>
			<p class="col-7"itemprop="description">${transform.overview}</p>
			<div class="col-10" itemprop="directors" itemscope itemtype="https://schema.org/Person">
				<p itemprop="directors">Réalisateur: ${transform.credits.crew[0].name}</p>
			</div>
			<div class="col-10">
				<time itemprop="datecreated" datetime="${transform.release_date}">date de sortie: ${new Date(transform.release_date).toLocaleDateString()}</time>
			</div>`;
			let casting = transform.credits.cast;
			console.log(casting);
        
			let containerCast = document.getElementById('casting');

			for (let i = 0; i<4; i++){
				let actor = casting[i];
          
				let actorProfile = document.createElement('div');
				actorProfile.classList.add('class','col-md-2');
				actorProfile.classList.add('class','col-5');
				actorProfile.classList.add('class','mr-2');
			
				actorProfile.innerHTML=`
            <img itemprop="image" src="https://image.tmdb.org/t/p/w500/${actor.profile_path}" alt="photo de ${actor.name}">
            <div itemprop="actor" itemscope itemtype="http://schema.org/Person">
            <p itemprop="name">${actor.name}</p>
            </div>`;
				containerCast.append(actorProfile);
			}
        
		});
}

