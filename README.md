# Application Good Movie 

## Présentation

Ce projet a été réalisé dans le cadre de ma formation. L'objectif était de créer un site internet de recherche d’informations sur des films. Grâce à ce site il est possible de rechercher n’importe quel film et de connaître toutes les informations liées au film (titre, durée, réalisateur, etc.).

Le site propose un champ de recherche qui permet de taper le titre d’un film et de faire une recherche sur la base. La recherche donne une liste des meilleures correspondances avec ce que l’utilisateur a tapé. Cette liste est scindée en plusieurs pages.

Chaque élément des résultats de recherche est cliquable et amène vers la page du film. Dans cette page, on affiche toutes les informations utiles pour le film.

Ce projet a aussi été l'occasion de prendre connaissance de certaines règles de référencement. En utilisant la documentation du site "www.schema.org" j’ai ajouté les attributs “itemprop”, “itemscope” et “itemtype” aux balises qui apportaient des informations sur chaque film. 
Puis, j’ai ajouté des balises meta dans le HTML , j’ai vérifié que les textes alternatifs étaient placés pour chaque image. J’ai vérifié la navigation au clavier sur mon site.

## Moyens utilisés

La maquette est réalisée avec Figma.
Le site est réalisé en HTML5, CSS3, JavaScript ES6+, Sass pour la création du CSS. J’ai utilisé npm pour la gestion des dépendances. J’ai  utilisé le framework CSS Bootstrap.
J’ai  utilisé ESLint pour la correction de la syntaxe du code.
Utilisation de l’API TheMovieDB ( cf le site: https://www.themoviedb.org).
